var exec = require('cordova/exec');

var HopinChathead = {
    init: function (success, error) {
        return exec(success, error, "HopinChathead", "init", []);
    },
    show: function (success, error) {
        return exec(success, error, "HopinChathead", "show", []);
    },
    hide: function (success, error) {
        return exec(success, error, "HopinChathead", "hide", []);
    },
    destroy: function (success, error) {
        return exec(success, error, "HopinChathead", "destroy", []);
    },
    hasPermission: function (success, error) {
        return exec(success, error, "HopinChathead", "hasPermission", []);
    },
    requestPermission: function (success, error) {
        return exec(success, error, "HopinChathead", "requestPermission", []);
    },
    moveTaskToBack: function (success, error) {
        return exec(success, error, "HopinChathead", "moveTaskToBack", []);
    },
    enableOverLockScreen: function (success, error) {
        return exec(success, error, "HopinChathead", "enableOverLockScreen", []);
    },
    disableOverLockScreen: function (success, error) {
        return exec(success, error, "HopinChathead", "disableOverLockScreen", []);
    },
    bringToFront: function (success, error) {
        return exec(success, error, "HopinChathead", "bringToFront", []);
    },
    bringToFront2: function (success, error) {
        return exec(success, error, "HopinChathead", "bringToFront2", []);
    },
};

module.exports = HopinChathead;